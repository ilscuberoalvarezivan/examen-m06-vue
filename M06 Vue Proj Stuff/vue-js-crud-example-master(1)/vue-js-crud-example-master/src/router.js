import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/tutorials",
      name: "tutorials",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/new",
      name: "novetats",
      component: () => import("./components/Novetats")
    },
    {
      path: "/cart",
      name: "cart",
      component: () => import("./components/CartList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddTutorial")
    },
    {
      path: "/addAtCart",
      name: "addAtCart",
      component: () => import("./components/AddAtCart")
    }
  ]
});
