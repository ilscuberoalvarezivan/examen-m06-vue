import http from "../http-common";

class TutorialDataService {
  getAll() {
    return http.get("/tutorials");
  }

  getCart() {
    return http.get("/cartmodels");
  }

  createForCart(data) {
    return http.post("/cartmodels", data);
  }

  addToTheCart(id, data) {
    return http.put(`/cartmodels/${id}`, data);
  }

  get(id) {
    return http.get(`/tutorials/${id}`);
  }

  create(data) {
    return http.post("/tutorials", data);
  }

  update(id, data) {
    return http.put(`/tutorials/${id}`, data);
  }

  delete(id) {
    return http.delete(`/tutorials/${id}`);
  }

  deleteCartElement(id) {
    return http.delete(`/cartmodels/${id}`);
  }

  deleteAll() {
    return http.delete(`/tutorials`);
  }

  deleteAllCart() {
    return http.delete(`/cartmodels`);
  }

  findByTitle(title) {
    return http.get(`/tutorials?title=${title}`);
  }

  findNew() {
    return http.get(`/tutorials?novetat=1`);
  }
}

export default new TutorialDataService();
